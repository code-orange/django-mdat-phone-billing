# Generated by Django 3.1.13 on 2021-08-19 13:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_phone_billing", "0003_routing_key_unique"),
    ]

    operations = [
        migrations.AddField(
            model_name="mdatphonebillingrouteitems",
            name="billing_type",
            field=models.IntegerField(
                choices=[(100, "per minute"), (200, "per call")], default=100
            ),
        ),
    ]
