from datetime import datetime, date

from django.db import models

from django_mdat_customer.django_mdat_customer.models import MdatItems, MdatCustomers
from django_mdat_phone.django_mdat_phone.models import MdatPhoneCountryCode


class MdatPhoneBillingCountryItems(models.Model):
    id = models.BigAutoField(primary_key=True)
    country_code = models.ForeignKey(
        MdatPhoneCountryCode, models.DO_NOTHING, unique=True
    )
    item_fixed_line = models.ForeignKey(MdatItems, models.DO_NOTHING, related_name="+")
    item_mobile = models.ForeignKey(MdatItems, models.DO_NOTHING, related_name="+")
    item_toll_free = models.ForeignKey(MdatItems, models.DO_NOTHING, related_name="+")
    date_added = models.DateTimeField(default=datetime.now)
    valid_from = models.DateField(default=date(1970, 1, 1))

    class Meta:
        db_table = "mdat_phone_billing_country_items"


class MdatPhoneBillingRouteItems(models.Model):
    BILLING_TYPES = [
        (100, "per minute"),
        (200, "per call"),
    ]

    IDENT_TYPES = [
        (100, "equal"),
        (200, "startswith"),
    ]

    id = models.BigAutoField(primary_key=True)
    routing_key = models.BigIntegerField(null=False, default=0)
    ident_type = models.IntegerField(choices=IDENT_TYPES, default=200)
    item = models.ForeignKey(MdatItems, models.DO_NOTHING, related_name="+")
    billing_type = models.IntegerField(choices=BILLING_TYPES, default=100)
    free_time = models.IntegerField(default=0)
    date_added = models.DateTimeField(default=datetime.now)
    valid_from = models.DateField(default=date(1970, 1, 1))

    class Meta:
        indexes = [
            models.Index(fields=["routing_key"]),
        ]
        unique_together = (("routing_key", "valid_from"),)
        db_table = "mdat_phone_billing_route_items"


class MdatPhoneBillingRouteCarrierZones(models.Model):
    id = models.BigAutoField(primary_key=True)
    carrier = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    carrier_code = models.CharField(max_length=100)
    item = models.ForeignKey(MdatItems, models.DO_NOTHING, related_name="+")
    date_added = models.DateTimeField(default=datetime.now)
    valid_from = models.DateField(default=date(1970, 1, 1))

    class Meta:
        unique_together = (("carrier", "carrier_code"),)
        db_table = "mdat_phone_billing_route_carrier_zones"
